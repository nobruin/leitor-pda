@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
      <div class="col-12">  
        <div class="panel-body">
           <div class="panel-group" id="accordion">                    
                @foreach ($directories as $nome)
                    <div class="panel panel-default ">
                        <div class="panel-heading" >
                            <ul class="list-group"id="list-tab" role="tablist">
                                <li class="list-group-item list-group-item-action" id="list-home-{{$nome}}" role="tab" >
                                    <input type="hidden" id="path_{{$nome}}" value="{{$nome}}"/>
                                    <a  id="{{ $nome }}"  class="pschema"  data-toggle="accordion-toggle collapsed" data-parent="#accordion" > {{($nome)}} </a>                                    
                                    <div id="N_{{ $nome }}" class="panel-collapse collapse level-0">  </div>
                                </li>                                
                            </ul>
                        </div>                        
                    </div>
                @endforeach
            </div>
        </div>
     </div>
 </div>    
</div> 
<script text="javascript">
$( document ).ready(function() {

    $('.pschema').click(function(){
        var id = this.id;
        tratarRequest(id);     
    });   
        
 });
 function tratarRequest(id, path){
    var ext = id.split('.')[1];
    
    if(!ext){        
        listadiretorios(id, path);        
        var level = typeof $('#level-'+id).val() == 'undefined' ?  0 : parseInt($('#level-'+id).val()); 
        
        $('.level-'+level).collapse('hide');
        $('#N_'+ id ).hasClass('in') ? $('#N_'+ id ).collapse('hide') : $('#N_'+ id ).collapse('toggle');
    }        
     else{
        downloadArquivo(id, path);
     }
        

    $('ul li').removeClass('active');
    $('#list-home-'+id).hasClass('active') ? $('#list-home-'+id).removeClass('active') : $('#list-home-'+id).addClass('active');   

 }

 function downloadArquivo(id, path){
    params = path ? id+'?path='+path : id;
    window.open('<?php echo URL::to('/'); ?>/' + params);
 }

 function listadiretorios(id, path){    
        params = path ? id+'?path='+path : id;
            if(!$('#lista-'+ id ).hasClass('list-group'))
            { 
                $("#loadMe").modal({
                    keyboard: false, //remove option to close with keyboard
                    show: true //Display loader!                
                });

                var level = typeof $('#level-'+id).val() == 'undefined' ?  1 : parseInt($('#level-'+id).val()) + 1; 

                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: '<?php echo URL::to('/'); ?>/' + params,
                    success: function (response)     {                                                
                        stringUls = "<ul id='lista-"+id+"' class='list-group'>"
                        if(response.files.length > 0){                            
                            $.each(response.files, function(key, val) {                               
                                stringUls += "<li class='list-group-item list-group-item-action pschema' id='list-home-"+val+"' level-"+level+"' >\n"+    
                                                "<input  id='level-"+val+"' type='hidden' value='"+level+"' />"+         
                                                 "<a  id='"+val+"' onclick=\"tratarRequest('"+val+"', '"+response.path+"')\" data-toggle='accordion-toggle collapsed' data-parent='#accordion' > "+val+" </a>"+                                    
                                                 "<div id='N_"+val+"' class='panel-collapse collapse level-"+level+"'> </div>"+
                                              "</li>\n";
                            });   
                        }else{
                            stringUls +="<li class='list-group-item list-group-item-action' >Folder vazio</li>";
                        }
                        stringUls +="</ul>"
                        
                        $('#N_'+ id ).html(stringUls);                    
                        $("#loadMe").modal("hide");                         

                    },
                    error: function () {
                        alert(response.message);
                    }    
                });        
        }
} 
    
</script>
@endsection

@extends('layouts.loading')


