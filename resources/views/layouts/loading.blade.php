<div class="modal fade" id="loadMe" tabindex="-1" role="dialog" aria-labelledby="loadMeLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
                <div class="d-flex align-items-center" >
                        <strong>Carregando...</strong>
                        <div class="spinner-border ml-auto" role="status" aria-hidden="true"></div>
               </div>               
            </div>          
          </div>
        </div>
 </div>