<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $pdaPath = '/var/www/html/FTP/PDA/';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
                
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $directories = array();
        $files = array();
        $directories = array_diff(\scandir($this->pdaPath), array('..', '.'));

        sort($directories);
        foreach ($directories as $key => $item) {
            if(!is_dir($this->pdaPath.$item)){
                $files[] = $item;
                unset($directories[$key]);
            }else{
                
            }
        }
        
        return view('home', array('directories' => $directories));
    }

    public function show(Request $request,String $name){

        if(!isset($name) || empty($name)){
            return "Diretorio inválido ou inexistente";
        }            

        $path = !$request->input('path') ? null : \str_replace('.', '/',$request->input('path'));
        $filePath = !$path ?  $this->pdaPath.$name : $this->pdaPath.$path.$name;
        
        if(!\is_dir($filePath) && !\is_file($filePath)){
            return response()->json([
                'message' => 'invalid directory',                
            ], 404);
            exit;
        }

        if(\is_dir($filePath)){
            $files = array_diff(\scandir($filePath), array('..', '.'));
            sort($files);
            return response()->json([
                'message' => 'sucess',
                'files' => $files,
                'path' => $request->input('path').$name.'.',
            ], 200);        
        }else{
            return response()->download($filePath);
        }
        
    }

}
